# ui_automation_test_program

@author: Carlos Arroyo Villalobos

Automation script in Python 3 for Windows 10 using PyAutoGui.

This script performs the next actions:

    • Launch the standard Windows 10 calculator application
    • Open a blank .txt file in Notepad
    • Perform the following using the calculator and log the operations and result of each step in the .txt file:
        ◦ Add 2 and 2
        ◦ Multiply the result by 5
        ◦ Subtract 10 from the result
    • Save the .txt file to the Desktop as Results1.txt
    • Open a new blank .txt file in Notepad
    • Clear the calculator and switch it to scientific mode
    • Change the computation mode from degree (DEG) to radian (RAD)
    • Perform the following using the calculator and log the operations and result of each step in the .txt file:
        ◦ Compute the cosine of pi
        ◦ Take the absolute value of the result
        ◦ Divide the result by 2
    • Save the .txt file to the Desktop as Results2.txt
    • Close the calculator and notepad

(THIS REPO ITS ONLY FOR DEMONSTRATION PURPOSES)

## Pre-requisites

* This script assumes you are using Windows 10 with US English language as default.

* Your keyboard distribution must be in US English. 

* This script uses python 3.x. If you don't have it installed, please visit 
[download python](https://www.python.org/downloads/windows/).

## Installation 

Clone this repository

With git on PowerShell:

	> git clone git@bitbucket.org:kangu_arrovi/ui_automation_test_program.git
	
Or download it from Google Drive [link](https://drive.google.com/drive/folders/15hd1Pm7EIWxxwAJrL17bH6cBwv08Mx-c?usp=sharing)

* Note: If you download it in this way, rename the project folder to 'ui_automation_test_program'.

Create a virtualenv (on Windows 10)

    > cd ui_automation_test_program
	> py -m venv venv
	> venv/Scripts/activate

Upgrade pip if it is required 

	> pip install --upgrade pip

Install requirements 

	> pip install -r requirements.txt
		
## Run in development

	> py ./automation.py

Stay away from mouse and keyboard and look the automatization process.
    
You may stop the script going to the PowerShell and type 'Ctrl' + C.

### Note
A demo video is attached into the project directory as evidence of correct behavior of the script. In that demo, the
computer configuration used was partially in Spanish. For that reason, the window name for the calculator was
'Calculadora' instead 'Calculator'. If you have a partial Spanish configuration like this, at line 19 of 'automation.py'
you may change 'Calculator' to 'Calculadora'.


