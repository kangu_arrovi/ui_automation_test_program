import os
import pyautogui
import time
import pyperclip

pyautogui.PAUSE = 1

# pyautogui.alert('Please. Do not use mouse and keyboard during the process.')

# Launch the standard Windows 10 calculator application
os.startfile("calc.exe")

# Open a blank .txt file in Notepad
os.startfile("notepad.exe")

time.sleep(2)

notepad = pyautogui.getWindowsWithTitle('Notepad')[0]
calculator = pyautogui.getWindowsWithTitle('Calculator')[0]


def copy_calc_result():
    # For coping the calculator result
    pyautogui.moveTo(calculator.left + 150, calculator.top + 150)
    pyautogui.click(button='right', clicks=1)
    time.sleep(1)
    pyautogui.press('enter')


calculator.activate()
time.sleep(1)

pyautogui.hotkey('alt', '1') # Enters into 'Calculator Standard Mode'.

# Perform the following using the calculator and log the operations and result of each step in the .txt file:

# Add 2 and 2
pyautogui.press('2')
pyautogui.press('+')
pyautogui.press('2')
pyautogui.press('enter')

copy_calc_result()

time.sleep(1)

# Logging on notepad
notepad.activate()
pyautogui.write("2 + 2 = ")
pyautogui.hotkey('ctrl', 'v')

pyautogui.write('\n')
pyautogui.hotkey('ctrl', 'v')

calculator.activate()
time.sleep(1)

# Multiply the result by 5
pyautogui.press('*')
pyautogui.press('5')
pyautogui.press('enter')

# For copy calc result
copy_calc_result()

# Logging on notepad
notepad.activate()
pyautogui.write(' X 5 = ')
pyautogui.hotkey('ctrl', 'v')

pyautogui.write('\n')
pyautogui.hotkey('ctrl', 'v')

# window change to calculator
calculator.activate()
time.sleep(1)

# Subtract 10 from the result
pyautogui.press('-')
pyautogui.press('1')
pyautogui.press('0')
pyautogui.press('enter')

copy_calc_result()

notepad.activate()
pyautogui.write(' - 10 = ')
pyautogui.hotkey('ctrl', 'v')

# Save the .txt file to the Desktop as Results1.txt
pyautogui.hotkey('ctrl', 's')
desktop = os.path.join(os.path.join(os.environ['USERPROFILE']), 'Desktop')
pyperclip.copy('{desktop}\Results1.txt'.format(desktop=desktop))
pyautogui.hotkey('ctrl', 'v')
time.sleep(1)
pyautogui.press('enter')

# Open a new blank .txt file in Notepad
pyautogui.hotkey('ctrl', 'n')

# window change to calculator
calculator.activate()
time.sleep(1)

# Clear the calculator and switch it to scientific mode
pyautogui.press('backspace')
pyautogui.hotkey('alt', '2')

# Change the computation mode from degree (DEG) to radian (RAD)
pyautogui.press('f4')

# Perform the following using the calculator and log the operations and result of each step in the .txt file:
# Compute the cosine of pi
pyautogui.press('p') # This is for enter Pi

# Copying the Pi value.
# For copy calc result in scientific mode. This method must be called twice.
copy_calc_result()
copy_calc_result()

notepad.activate()
pyautogui.write('cos(')
pyautogui.hotkey('ctrl', 'v')
pyautogui.write(') = ')

calculator.activate()
time.sleep(1)

# Calculate cos()
pyautogui.press('o')

# For copy calc result in scientific mode. This method must be called twice.
copy_calc_result()
copy_calc_result()

# Logging result
notepad.activate()
pyautogui.hotkey('ctrl', 'v')
pyautogui.write('\n')

# Take the absolute value of the result
pyautogui.write('abs(')
pyautogui.hotkey('ctrl', 'v')
pyautogui.write(') = ')

calculator.activate()
time.sleep(1)

# Calculate abs()
pyautogui.hotkey('shift', '\\')

# For copy calc result in scientific mode. This method must be called twice.
copy_calc_result()
copy_calc_result()

# Logging result
notepad.activate()
pyautogui.hotkey('ctrl', 'v')
pyautogui.write('\n')

# Divide the result by 2
pyautogui.hotkey('ctrl', 'v')
pyautogui.write(' / 2 = ')

calculator.activate()
time.sleep(2)

# Logging
pyautogui.press('/')
pyautogui.press('2')
pyautogui.press('enter')

# For copy calc result in scientific mode. This method must be called twice.
copy_calc_result()
copy_calc_result()

# Logging result
notepad.activate()
pyautogui.hotkey('ctrl', 'v')
pyautogui.write('\n')

# Save the .txt file to the Desktop as Results2.txt
pyautogui.hotkey('ctrl', 's')
desktop = os.path.join(os.path.join(os.environ['USERPROFILE']), 'Desktop')
pyperclip.copy('{desktop}\Results2.txt'.format(desktop=desktop))
pyautogui.hotkey('ctrl', 'v')
time.sleep(1)
pyautogui.press('enter')

# Close the calculator and notepad
calculator.activate()
pyautogui.hotkey('alt', 'f4')  # Closes calculator

notepad.activate()
pyautogui.hotkey('alt', 'f4')  # Closes notepad
